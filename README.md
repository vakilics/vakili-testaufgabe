# Vakili-Testaufgabe
Following, I provided the possible solutions to the task test (Testaufgabe) based on my understanding of each part. 

## 1. Installation of UCS (Core Edition)
As it was recommended by Univention website, I used stable UCS 4.4 version. 
- UCS 4.4 VM-Images:  UCS-4.4-VM-Image.ova
- Server role and version:
```
root@ucs-2806:~# ucr get server/role
domaincontroller_master

``` 
```
root@ucs-2806:~# ucr search --brief version
appcenter/apps/kde/version: 5.8
notifier/protocol/version: 3
repository/mirror/version/end: <empty>
repository/mirror/version/start: <empty>
repository/online/component/.*/version: <empty>
repository/online/component/4.4-8-errata/version: 4.4
update/umc/nextversion: true
version/erratalevel: 1158
version/patchlevel: 8
version/releasename: Blumenthal
version/version: 4.4
```
# 2. Interfaces of product
Before creating 100 test users, I just tested if I can create a new user “vakili1”: 
```
root@ucs-2806:~# udm users/user create --set username=vakili1 \
  --set  firstname=AbdulRahman \
  --set lastname=vakili \
  --set password=P@ssw0rd 
WARNING: The object is not going to be created underneath of its default containers.
Object created: uid=vakili1,dc=itcx,dc=intranet
```
To check:
```
root@ucs-2806:~# udm users/user list --filter uid=vakili1 | grep name 
  firstname: AbdulRahman 
  lastname: vakili 
  username: vakili1
```
I could also give more information: 
```
root@ucs-2806:~# univention-directory-manager users/user create \
 --position "cn=users,dc=itcx,dc=intranet \ 
 --set username="vakili1" \
 --set firstname="AbdulRahman" \
 --set lastname="Vakili" \
 --set organisation="Hopefully Univention" \
 --set mailPrimaryAddress="vakili.hu.it@gmail.com" \
 --set password="P@ssw0rd"
```
## 2.1. Generating 100 test accounts
There are ways of auto creation of users using command line, scripting in a file, pythoning or others. I just used the most simple way in a command line for simplicity as follow:
```
root@ucs-2806:~#  for num in {1..100}; do udm users/user create \
 --set username=testuser$num \
 --set firstname=testname$num \
 --set lastname=testlastname$num \
 --set password=P@ssw0rd; done
```
## 2.2. Modifying test account
Modifying a user in UCS, is possible using “udm users/user modify”. 

I noticed that changes to take effect, it sometimes require to restart univention-directory-manager (udm) to manage UCS LDAP or univention-config-registry (ucr) for base configuration of UCS.

Following methods, we modify user “testuser1” to be privileged user for administration task:

### 2.2.1. Domain Admins 
Administrator account is member of provileged group "Domain Admins". Adding any user to this group simply gives the power! 

Let's add "testuser1" to the “Domain Admins” group:
```
root@ucs-2806:~# univention-directory-manager groups/group modify \
 --dn "cn=Domain Admins,cn=groups,dc=itcx,dc=intranet" \
 --append users="uid=testuser1,dc=itcx,dc=intranet"
Object modified: cn=Domain Admins,cn=groups,dc=itcx,dc=intranet
```
To check: 
```
root@ucs-2806:~# udm users/user list --filter uid=testuser1 | grep groups
  groups: cn=Domain Admins,cn=groups,dc=itcx,dc=intranet
  groups: cn=Domain Users,cn=groups,dc=itcx,dc=intranet
  primaryGroup: cn=Domain Users,cn=groups,dc=itcx,dc=intranet

```

### 2.2.2. Creating “Portal Admins”

Based on Univention documents, we can create a privileged group, add the new policy and then add the user to the group to grant desired policy: 

 <b>Add "Portal Admins" group:</b>
```
root@ucs-2806:~# udm groups/group create \
 --position="cn=groups,$(ucr get ldap/base)" \
 --set name="Portal Admins" 
Object created: cn=Portal Admins,cn=groups,dc=itcx,dc=intranet

```
<b>Add testuser1 to "Portal Admins":</b>
```
root@ucs-2806:~# univention-directory-manager groups/group modify \
 --dn "cn=Portal Admins,cn=groups,dc=itcx,dc=intranet" \
 --append users="uid=testuser1,cn=users,dc=itcx,dc=intranet" 
Object modified: cn=Portal Admins,cn=groups,dc=itcx,dc=intranet
```
<b>Assign rights for Portal settings:</b>

 Based on documentation
 Created ACL file:
```
root@ucs-2806:~# cat /opt/privileged-portal-acl.acl
```
```
 access to dn="cn=portal,cn=univention,@%@ldap/base@%@" attrs=children
 by group/univentionGroup/uniqueMember="cn=Portal Admins,cn=groups,@%@ldap/base@%@" write
 by * +0 break

 access to dn.children="cn=portal,cn=univention,@%@ldap/base@%@" attrs=entry,@univentionObject,
 @univentionPortalEntry, @univentionPortal,@univentionPortalCategory,children
 by group/univentionGroup/uniqueMember="cn=Portal Admins,cn=groups,@%@ldap/base@%@" write
 by * +0 break
```
Then, create an LDAP object for the LDAP ACLs:
```
root@ucs-2806:~# udm settings/ldapacl create 
 --position "cn=ldapacl,cn=univention,$(ucr get ldap/base)" 
 --set name=privileged-portal-acl 
 --set filename=privileged-portal-acl 
 --set data="$(bzip2 -c /opt/privileged-portal-acl.acl | base64)" 
 --set package="privileged-portal-acl" 
 --set packageversion=1 
Object created: cn=privileged-portal-acl,cn=ldapacl,cn=univention,dc=itcx,dc=intranet
```
We also add/edit/enable modules to be available to the testuser1 in "Policies -> Policy:UMC " tab of the group policy in Univention Portal via GUI.  

## 2.3. Check test accounts
Using tools to check successful creation of test users:

<b>Using: "univention-ldapsearch"</b> 
```
root@ucs-2806:~# univention-ldapsearch | grep uid=testuser | grep uniqueMember \
| awk '{++cnt} END {print "Num_OF_TestUsers = ",cnt""}' ALL-TestUsers.txt 

Num_OF_TestUsers =  100
```
<b>Check by "slapcat"</b>

The LDAP Directory Interchange Format (LDIF): 
```
root@ucs-2806:~# slapcat | grep uid=testuser* | head
uniqueMember: uid=testuser1,dc=itcx,dc=intranet
uniqueMember: uid=testuser2,dc=itcx,dc=intranet
uniqueMember: uid=testuser3,dc=itcx,dc=intranet
uniqueMember: uid=testuser4,dc=itcx,dc=intranet
uniqueMember: uid=testuser5,dc=itcx,dc=intranet
uniqueMember: uid=testuser6,dc=itcx,dc=intranet
uniqueMember: uid=testuser7,dc=itcx,dc=intranet
uniqueMember: uid=testuser8,dc=itcx,dc=intranet
uniqueMember: uid=testuser9,dc=itcx,dc=intranet
uniqueMember: uid=testuser10,dc=itcx,dc=intranet

```
<b>Get first 10 users using "passwd"</b>

```
root@ucs-2806:~# getent passwd | grep testuser | head
testuser1:x:2114:5001:testname1 testlastname1:/home/testuser1:/bin/bash
testuser2:x:2115:5001:testname2 testlastname2:/home/testuser2:/bin/bash
testuser3:x:2116:5001:testname3 testlastname3:/home/testuser3:/bin/bash
testuser4:x:2117:5001:testname4 testlastname4:/home/testuser4:/bin/bash
testuser5:x:2118:5001:testname5 testlastname5:/home/testuser5:/bin/bash
testuser6:x:2119:5001:testname6 testlastname6:/home/testuser6:/bin/bash
testuser7:x:2120:5001:testname7 testlastname7:/home/testuser7:/bin/bash
testuser8:x:2121:5001:testname8 testlastname8:/home/testuser8:/bin/bash
testuser9:x:2122:5001:testname9 testlastname9:/home/testuser9:/bin/bash
testuser10:x:2123:5001:testname10 testlastname10:/home/testuser10:/bin/bash
```
# 3. Installation of additional software

As the Univention is based on Debian, we can install any “.deb” software packages.
## Install as standard package
Install via standard package manager and run it: 
```
root@ucs-2806:~# univention-install google-chrome-stable
```
## Install and run in APP Center
Based on Univention [App Tutorial](https://docs.software-univention.de/app-tutorial-4.1.html#docker) document, we can do app development using docker image. Either create a package or use the available docker image for it. Then can populate the app in to Univention App Center. 
### Repository activation
As the additional software like Google Chrome will be installed by Univerntion repository and not App Center, therefore we enable the “unmaintained” repository:
```         
root@ucs-2806:~# ucr set repository/online/unmaintained='yes'	
```
### Install required packages:
```
root@ucs-2806:~# univention-install univention-appcenter
root@ucs-2806:~# univention-install univention-appcenter-docker
root@ucs-2806:~# univention-install univention-appcenter-dev
```
### Create App ini file
As each App, has its own .ini file, we create an this file to declare metadata inside it:

```
root@ucs-2806:~# cat chrome.ini
[Application]
ID = google-apps
Code = GA
Name = Chrome
Version = 74.0
Vendor=Google
WebsiteVendor=https://www.google.com/chrome/
Maintainer=Univention
WebsiteMaintainer=http://www.univention.de/
Categories=Collaboration
DefaultPackages=google-chrome-stable
DockerImage=docker.software-univention.de/ucs-appbox-amd64:4.3-3
#PortsExclusive=8080

```
### Populate into App Center
```
root@ucs-2806:~# univention-app dev-populate-appcenter --new --ini chrome.ini --unmaintained google-chrome-stable

```
Bellow we can see that chrome App is already added into App Center and ready to be installed: 
	
[Ready to install](https://gitlab.com/-/ide/project/vakilics/vakili-testaufgabe/tree/main/-/imgs/1-Chrome-Ready-to-install.png)

[Installing...](https://gitlab.com/-/ide/project/vakilics/vakili-testaufgabe/tree/main/-/imgs/2-Chrome-Installing.png)

[Installed](https://gitlab.com/-/ide/project/vakilics/vakili-testaufgabe/tree/main/-/imgs/3-Chrome-Installed.png)


# 4. Technical documentation
I used "gitlab" to document solutions to each part of task test. 

[README.md](https://gitlab.com/-/ide/project/vakilics/vakili-testaufgabe/tree/main/-/README.md/)

[PDF](https://gitlab.com/-/ide/project/vakilics/vakili-testaufgabe/tree/main/-/Aufgabe-L%C3%B6sung.pdf)
